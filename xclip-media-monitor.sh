#!/usr/bin/env bash

# Output path and filename pattern
OUTPUT="~/Downloads/%(title)s.%(ext)s"
# Path for the temporary file
TEMP="/tmp/xclip-links.txt"
# Clipboard check interval (in seconds)
CHECKINTERVAL=1

# Variable for determining if Cancel has been selected
BYE=0

# Clean up and exit after `pkill -f -USR2 "bash .*xclip-ytdl-monitor.sh"` is run
clean() {
	rm $TEMP && exit 0
}

# The download process and clean up start after `pkill -f -SIGTRAP "bash .*xclip-ytdl-monitor.sh"` is run
dl() {
	BYE=1
	notify-send -t 2000 'Media Link Monitor' "Download started as \"$OUTPUT\"…"
	youtube-dl --external-downloader=aria2c --write-sub --all-subs --add-metadata -o $OUTPUT -a $TEMP
	notify-send -t 2000 'Media Link Monitor' 'Download finished'
	clean
}

# Play with mpv and clean up after `pkill -f -USR1 "bash .*xclip-ytdl-monitor.sh"` is run
play() {
	BYE=1
	notify-send -t 2000 'Media Link Monitor' 'Opening with mpv…'
	mpv --playlist=$TEMP
	clean
}

trap dl SIGTRAP
trap play USR1
trap clean USR2

# When clipboard content changes, add it to temporary file
while [[ true ]]; do
	# If the TRAP signal caught, stop monitoring
	(( $BYE == 1 )) &&  break
	LINK=$(xclip -selection clipboard -o)
	[[ -f $TEMP ]] || touch $TEMP
	if ! [[ $LINK == $(tail -n 1 $TEMP) ]] ; then
		echo $LINK >> $TEMP
		notify-send -t 2000 --icon download 'Media Link Monitor' 'New link added'
	fi
	# Clipboard check inverval
	sleep $CHECKINTERVAL
done
