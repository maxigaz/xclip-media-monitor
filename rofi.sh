#!/usr/bin/env bash

# Pass options to dmenu (or symlinked rofi), then choose action
x=$(echo -e "Start monitoring <span size='x-small'>(1)</span>|\
Download and quit <span size='x-small'>(2)</span>|\
Open with mpv and quit <span size='x-small'>(3)</span>|\
Cancel <span size='x-small'>(4)</span>|\
Cancel (keep temp file) <span size='x-small'>(5)</span>" \
| rofi -dmenu -sep '|' -markup-rows -format d -i -p "Video link monitoring tool")

case "$x" in
	'1')
		$(dirname "$0")/xclip-media-monitor.sh ;;
	'2')
		pkill -f -SIGTRAP "bash .*xclip-media-monitor.sh" ;;
	'3')
		pkill -f -USR1 "bash .*xclip-media-monitor.sh" ;;
	'4')
		pkill -f -USR2 "bash .*xclip-media-monitor.sh" ;;
	'5')
		pkill -f "bash .*xclip-media-monitor.sh" ;;
esac
