# XClip Media Monitor

Bash scripts for [Rofi](https://github.com/DaveDavenport/rofi) that periodically check the content of your clipboard so you that can queue the URLs added to it individually for downloading or playing.

## Dependencies

- a Bash-compatible shell
- xclip
- youtube-dl
- mpv
- libnotify

## Usage/How It Works

1. Copy the URL of a media link to your clipboard (it can be a video from YouTube or PeerTube, or even a local one, for example).
2. Run `rofi.sh` and select `Start monitoring`. (This will run `xclip-media-monitor.sh`.)
3. Now the URL is added to the queue, which is placed in a temporary file.
4. Copy more URLs to the clipboard. They are added to the queue as well. (A desktop notification appears each time this happens.)
5. When you’ve finished adding the URLs of your choice, run `rofi.sh` again and select `Download and quit` or `Open with mpv and quit`.
6. Depending on your choice, all the media from the URLs you’ve added start downloading to the specified location (see below) or are opened in mpv as a playlist.
7. In case you change your mind and want to cancel monitoring, you can do so by running `rofi.sh` and selecting `Cancel` or `Cancel (keep temp file)`.

## Options

`xclip-media.monitor.sh` contains three variables for easy customization:

```sh
# youtube-dl output template
OUTPUT="~/Downloads/%(title)s.%(ext)s"
# Path for the temporary file
TEMP="/tmp/xclip-links.txt"
# Clipboard check interval (in seconds)
CHECKINTERVAL=1
```

You can read more about youtube-dl’s output template in `man youtube-dl`.

## Caveats

Currently, the script doesn’t check whether your clipboard contains a valid URL or not. At one point I was planning on implementing a filter for YouTube links, but then I realized that this link monitor could be useful for links pointing *anywhere* on the web, not just YouTube. Also, not having any filters allows us to quickly open arbitrary local files in mpv.
